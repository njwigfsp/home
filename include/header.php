        <nav class="navbar navbar-expand-lg fixed-top custom-nav sticky">
            <div class="container">
                <a class="logo navbar-brand" href="/">
                    <img src="/include/img/logo.jpg" class="img-fluid logo-light" alt="logo Arcmovie " title="logo Arcmovie "> 
                    <img src="/include/img/logo.jpg" class="img-fluid logo-dark" alt="logo Arcmovie " title="logo Arcmovie ">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="mdi mdi-menu"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul class="navbar-nav ml-auto navbar-center" id="mySidenav">
                        <li class = "nav-item">
                            <a href="/" class="nav-link"> Home </a>
                    </li>
                    </ul>
                </div>
            </div>
        </nav>
